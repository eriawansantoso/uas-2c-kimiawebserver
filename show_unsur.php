<?php

$DB_NAME = "kimia";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $nama_unsur = $_POST['nama_unsur'];
    $sql = "SELECT u.no_atom, u.simbol, u.nama_unsur, u.massa_atom, u.electron, u.proton, u.neutron, 
    g.nama_gol,u.keterangan, 
            concat('http://192.168.43.104/uas-2c-kimiawebserver/images/',u.photos) as url
            from unsur u, golongan g 
            where u.id_gol = g.id_gol
            and u.nama_unsur like '%$nama_unsur%'";

    
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_unsur = array();
        while ($unsur = mysqli_fetch_assoc($result)) {
            array_push($data_unsur, $unsur);
        }
        echo json_encode($data_unsur);
    }
}
