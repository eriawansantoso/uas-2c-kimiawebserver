/*
 Navicat Premium Data Transfer

 Source Server         : rizkadewi
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : kimia

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 16/05/2020 15:54:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for elektrolit
-- ----------------------------
DROP TABLE IF EXISTS `elektrolit`;
CREATE TABLE `elektrolit`  (
  `id_elektrolit` int(11) NOT NULL AUTO_INCREMENT,
  `nama_larutan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_elektrolit` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_elektrolit`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of elektrolit
-- ----------------------------
INSERT INTO `elektrolit` VALUES (1, 'Asamqq', 'Lemah');
INSERT INTO `elektrolit` VALUES (6, 'Asamqo', 'Lemah');

-- ----------------------------
-- Table structure for golongan
-- ----------------------------
DROP TABLE IF EXISTS `golongan`;
CREATE TABLE `golongan`  (
  `id_gol` int(11) NOT NULL AUTO_INCREMENT,
  `nama_gol` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_gol`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of golongan
-- ----------------------------
INSERT INTO `golongan` VALUES (1, 'I A');
INSERT INTO `golongan` VALUES (2, 'II A');
INSERT INTO `golongan` VALUES (3, 'III A');
INSERT INTO `golongan` VALUES (4, 'IV A');
INSERT INTO `golongan` VALUES (5, 'V A');
INSERT INTO `golongan` VALUES (6, 'VI A');
INSERT INTO `golongan` VALUES (7, 'VII A');
INSERT INTO `golongan` VALUES (8, 'VIII A');
INSERT INTO `golongan` VALUES (9, 'I B');
INSERT INTO `golongan` VALUES (10, 'II B');
INSERT INTO `golongan` VALUES (11, 'III B');
INSERT INTO `golongan` VALUES (12, 'IV B');
INSERT INTO `golongan` VALUES (13, 'V B');
INSERT INTO `golongan` VALUES (14, 'VI B');
INSERT INTO `golongan` VALUES (15, 'VII B');
INSERT INTO `golongan` VALUES (16, 'VIII');

-- ----------------------------
-- Table structure for senyawa
-- ----------------------------
DROP TABLE IF EXISTS `senyawa`;
CREATE TABLE `senyawa`  (
  `id_senyawa` int(255) NOT NULL AUTO_INCREMENT,
  `kation` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `anion` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rumus_senyawa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_senyawa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_senyawa`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of senyawa
-- ----------------------------
INSERT INTO `senyawa` VALUES (2, 'Na', 'Cl-', 'NaCl', 'natrium Clorida');
INSERT INTO `senyawa` VALUES (3, 'H', 'O-Â²', 'HÂ²O', 'Air/Aqua');

-- ----------------------------
-- Table structure for unsur
-- ----------------------------
DROP TABLE IF EXISTS `unsur`;
CREATE TABLE `unsur`  (
  `no_atom` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `simbol` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_unsur` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT ' ',
  `massa_atom` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `electron` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `proton` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `neutron` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_gol` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `photos` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`no_atom`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of unsur
-- ----------------------------
INSERT INTO `unsur` VALUES ('1', 'H', 'Hidrogen', '101', '1', '2', '1', 1, 'nonmetal', 'DC20200515002206.jpg');
INSERT INTO `unsur` VALUES ('2', 'Li', 'Litium', '9', '1', '9', '1', 1, 'nonmetal', 'DC20200514234044.jpg');
INSERT INTO `unsur` VALUES ('3', 'Na', 'Natrium', '1000', '1', '-1', '0', 1, 'nonmetal', 'DC20200515203927.jpg');

SET FOREIGN_KEY_CHECKS = 1;
