<?php

$DB_NAME = "kimia";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $mode = $_POST['mode'];
    $respon = array(); $respon['kode'] = '000';
    switch($mode){
        case "insert":
            $no_atom    = $_POST['no_atom'];
            $simbol       = $_POST['simbol'];
            $nama_unsur = $_POST['nama_unsur'];
            $massa_atom = $_POST['massa_atom'];
            $electron   = $_POST['electron'];
            $proton = $_POST['proton'];
            $neutron = $_POST['neutron'];
            $nama_gol = $_POST['nama_gol'];
            $keterangan  = $_POST['keterangan'];
            $imstr      = $_POST['image'];
            $file       = $_POST['file'];
            $path       = "images/";

            $sql = "select id_gol from golongan where nama_gol = '$nama_gol'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_gol = $data['id_gol'];

                $sql = "insert into unsur(no_atom, simbol, nama_unsur, massa_atom,
                electron, proton, neutron, id_gol, keterangan, photos) 
                values('$no_atom','$simbol','$nama_unsur','$massa_atom','$electron',
                '$proton','$neutron','$id_gol','$keterangan','$file')";

                $result = mysqli_query($conn,$sql);
                if($result){
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $sql = "delete from unsur where no_atom = '$no_atom'";
                        mysqli_query($conn,$sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        echo json_encode($respon); exit();
                    }
                }else{
                    $respon['kode'] = "111";
                    echo json_encode($respon); exit();
                }
            }
        break;
        case "update":
            $no_atom    = $_POST['no_atom'];
            $simbol       = $_POST['simbol'];
            $nama_unsur = $_POST['nama_unsur'];
            $massa_atom = $_POST['massa_atom'];
            $electron   = $_POST['electron'];
            $proton = $_POST['proton'];
            $neutron = $_POST['neutron'];
            $nama_gol = $_POST['nama_gol'];
            $keterangan  = $_POST['keterangan'];
            $imstr      = $_POST['image'];
            $file       = $_POST['file'];
            $path       = "images/";

            $sql = "select id_gol from golongan where nama_gol = '$nama_gol'";
            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_gol = $data['id_gol'];

                $sql = "";
                if($imstr == ""){
                    $sql = "update unsur set simbol='$simbol',nama_unsur='$nama_unsur',massa_atom='$massa_atom', electron='$electron',
                        proton='$proton',neutron='$neutron',id_gol='$id_gol',keterangan='$keterangan' where no_atom='$no_atom'"; 
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit();
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }else{
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        $sql = "update unsur set simbol='$simbol',nama_unsur='$nama_unsur',massa_atom='$massa_atom', electron='$electron',
                        proton='$proton',neutron='$neutron',id_gol='$id_gol',keterangan='$keterangan',photos='$file' where no_atom='$no_atom'";
                       
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }
                }
            }
        break;
        case "delete":
            $no_atom =$_POST['no_atom'];
            $sql ="select photos from unsur where no_atom='$no_atom'";
            $result = mysqli_query($conn,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    $path = "images/";
                    unlink($path.$photos);
                }
                $sql = "delete from unsur where no_atom = '$no_atom'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    echo json_encode($respon); exit();
                }else{
                    $respon['kode'] = '111';
                    echo json_encode($respon); exit();
                }
            }
        break;
    }
}