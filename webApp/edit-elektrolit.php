<?php 
    include("koneksi.php");
    if (!isset($_GET['id_elektrolit'])) {
		header('Location:index_senyawa.php');
    }
    $id = $_GET['id_elektrolit'];
    $sql = "SELECT * from elektrolit where id_elektrolit=$id";
	$query = mysqli_query($conn, $sql);
    $elektrolit = mysqli_fetch_assoc($query);
    if (mysqli_num_rows($query) < 1) {
		die("data tidak ditemukan...");
	}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Unsur</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">CHemistry</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Unsur Kimia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_senyawa.php">Senyawa Kimia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_elektrolit.php">Larutan Elektrolit<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
    </nav>

    <div class="container">
        <header>
            <h1><center>Edit Tabel Elektrolit<center></h1>
        </header>
        <form action="proses-edit-elektrolit.php" method="POST" enctype="multipart/form-data">
            <fieldset>
                            <input type="hidden" class="form-control" id="id_elektrolit" name="id_elektrolit" value="<?php echo $elektrolit['id_elektrolit'];?>">
                            <div class="form-group">
                                <label for="nama_larutan" class="col-form-label">Nama Larutan </label>
                                <input type="text" class="form-control" id="nama_larutan" name="nama_larutan" value="<?php echo $elektrolit['nama_larutan'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="jenis_elektrolit" class="col-form-label">Jenis Elektrolit </label>
                                <input type="text" class="form-control" id="jenis_elektrolit" name="jenis_elektrolit" value="<?php echo $elektrolit['jenis_elektrolit'];?>" required>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                            </div>
            </fieldset>
        </form>

    </main>
    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Aplikasi Pembelajaran Kimia ERIzka</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>