<?php
   include("koneksi.php");
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Senyawa</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">CHemistry</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Unsur Kimia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_senyawa.php">Senyawa Kimia<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_elektrolit.php">Larutan Elektrolit</a>
                    </li>
                </ul>
            </div>
        </nav>
    <div class="container">
       
        <h4><center>Data Senyawa<center></h4>
            
            <!-- MODAL INSERT -->
       <button  type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Tambah Senyawa</button>
       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
               <div class="modal-content">
                   <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">Tambahkan Data</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                       </button>
                   </div>
                   <div class="modal-body">
                       <form action="proses-tambah-senyawa.php" method="POST" enctype="multipart/form-data">
                           <div class="form-group">
                               <label for="kation" class="col-form-label">Kation</label>
                               <input type="text" class="form-control" id="kation" name="kation" required>
                           </div>
                           <div class="form-group">
                               <label for="anion" class="col-form-label">Anion</label>
                               <textarea class="form-control" id="anion" name="anion" required></textarea>
                           </div>
                           <div class="form-group">
                               <label for="rumus_senyawa" class="col-form-label">Rumus Senyawa</label>
                               <textarea class="form-control" id="rumus_senyawa" name="rumus_senyawa" required></textarea>
                           </div>
                           <div class="form-group">
                               <label for="nama_larutan" class="col-form-label">Nama Larutan</label>
                               <textarea class="form-control" id="nama_larutan" name="nama_larutan" required></textarea>
                           </div>
                           <div class="modal-footer">
                               <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                               <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
       <!-- END MODAL  -->
            <table class="mt-3 table table-bordered">
                <tr>
                    <th>Id Senyawa</th>
                    <th>Kation</th>
                    <th>Anion</th>
                    <th>Rumus Senyawa</th>
                    <th>Nama Senyawa</th>
                    <th>Opsi</th>
                </tr>
                <?php 
                  $sql = "select * from senyawa order by id_senyawa asc";
    
                  $result = mysqli_query($conn,$sql);
                    while($pst = mysqli_fetch_assoc($result)){
                ?>
                <tr>                 
                    <td><?php echo $pst['id_senyawa']; ?></td>
                    <td><?php echo $pst['kation']; ?></td>
                    <td><?php echo $pst['anion']; ?></td>
                    <td><?php echo $pst['rumus_senyawa']; ?></td>
                    <td><?php echo $pst['nama_senyawa']; ?></td>
                    <td> 
                    <a href="edit-senyawa.php?id_senyawa=<?php echo $pst['id_senyawa']?>">
                        <button class="btn btn-xs btn-info tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Edit</button>
                    </a>
                    <a href="proses-hapus.php?id_senyawa=<?php echo $pst['id_senyawa']?>" title="Hapus Data Ini" onclick="return confirm('Yakin Hapus Data ?')">
                        <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-remove icon-white"></i>Hapus</button>
                    </a> 
                    </td>
                </tr> 
                <?php } ?>
            </table>
        </div>
    </div>
    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Aplikasi Pembelajaran Kimia ERIzka</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>