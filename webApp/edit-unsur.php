<?php 
    include("koneksi.php");
    if (!isset($_GET['no_atom'])) {
		header('Location:index.php');
    }
    $id = $_GET['no_atom'];
    $sql = "SELECT * from unsur where no_atom=$id";
	$query = mysqli_query($conn, $sql);
    $unsur = mysqli_fetch_assoc($query);
    if (mysqli_num_rows($query) < 1) {
		die("data tidak ditemukan...");
	}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Unsur</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">CHemistry</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Unsur Kimia<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_senyawa.php">Senyawa Kimia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_elektrolit.php">Larutan Elektrolit</a>
                    </li>
                </ul>
            </div>
    </nav>

    <div class="container">
        <header>
            <h1><center>Edit Tabel Unsur<center></h1>
        </header>
        <form action="proses-edit-unsur.php" method="POST" enctype="multipart/form-data">
            <fieldset>
                            <input type="hidden" class="form-control" id="no_atom" name="no_atom" value="<?php echo $unsur['no_atom'];?>">
                            <div class="form-group">
                                <label for="simbol" class="col-form-label">Simbol :</label>
                                <input type="text" class="form-control" id="simbol" name="simbol" value="<?php echo $unsur['simbol'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="nama_unsur" class="col-form-label">Nama Unsur :</label>
                                <input type="text" class="form-control" id="nama_unsur" name="nama_unsur" value="<?php echo $unsur['nama_unsur'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="massa_atom" class="col-form-label">Massa Atom :</label>
                                <input type="text" class="form-control" id="massa_atom" name="massa_atom" value="<?php echo $unsur['massa_atom'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="electron" class="col-form-label">Electron :</label>
                                <input type="text" class="form-control" id="electron" name="electron" value="<?php echo $unsur['electron'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="proton" class="col-form-label">Proton :</label>
                                <input type="text" class="form-control" id="proton" name="proton" value="<?php echo $unsur['proton'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="neutron" class="col-form-label">Neutron :</label>
                                <input type="text" class="form-control" id="neutron" name="neutron" value="<?php echo $unsur['neutron'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="id_gol" class="col-form-label">Id Golongan :</label>
                                <input type="text" class="form-control" id="id_gol" name="id_gol" value="<?php echo $unsur['id_gol'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-form-label">Keterangan :</label>
                                <input type="text" class="form-control" id="keterangan" name="keterangan" value="<?php echo $unsur['keterangan'];?>" required>
                            </div>
                            <div style="padding-bottom: 10px">
                                    <img src='../images/<?php echo $unsur['photos'] ?>' width='100' height='100' />   
                                </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Gambar</span>
                                </div>
                                <div class="custom-file">
                                    <input type="hidden" name="images_lama" value="<?php echo $unsur['photos'];?>">
                                    <input type="file" class="custom-file-input" id="photos" name="photos">
                                    <label class="custom-file-label" for="photos">Pilih Gambar</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                            </div>
            </fieldset>
        </form>

    </main>
    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Aplikasi Pembelajaran Kimia ERIzka</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>