<?php
   include("koneksi.php");
   $sql = "SELECT * FROM golongan";
	
	$result = mysqli_query($conn,$sql);
   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Unsur</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">CHemistry</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Unsur Kimia<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_senyawa.php">Senyawa Kimia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_elektrolit.php">Larutan Elektrolit</a>
                    </li>
                </ul>
            </div>
        </nav>
    <div class="container">
       
         <h4><center>Data Unsur<center></h4>
            
             <!-- MODAL INSERT -->
        <button  type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Tambah Unsur</button>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambahkan Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="proses-tambah-unsur.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="no_atom" class="col-form-label">No Atom</label>
                                <input type="text" class="form-control" id="no_atom" name="no_atom" required>
                            </div>
                            <div class="form-group">
                                <label for="simbol" class="col-form-label">Simbol</label>
                                <textarea class="form-control" id="simbol" name="simbol" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="nama_unsur" class="col-form-label">Nama Unsur</label>
                                <textarea class="form-control" id="nama_unsur" name="nama_unsur" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="massa_atom" class="col-form-label">Massa Atom</label>
                                <textarea class="form-control" id="massa_atom" name="massa_atom" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="electron" class="col-form-label">Electron</label>
                                <textarea class="form-control" id="electron" name="electron" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="proton" class="col-form-label">Proton</label>
                                <textarea class="form-control" id="proton" name="proton" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="neutron" class="col-form-label">Neutron</label>
                                <textarea class="form-control" id="neutron" name="neutron" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="id_gol" class="col-form-label">Golongan</label>
                                <select class="form-control" name="id_gol" required>
                                    <?php while($row1 = mysqli_fetch_row($result)):; ?>
                                    <option value="<?php echo $row1[0]; ?>"><?php echo $row1[1]; ?></option>
                                    <?php endwhile; ?>
                                </select>
                            </div>
                    
                            <div class="form-group">
                                <label for="keterangan" class="col-form-label">Keterangan</label>
                                <textarea class="form-control" id="keterangan" name="keterangan" required></textarea>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Gambar</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="photos" name="photos" required>
                                    <label class="custom-file-label" for="photos">Pilih Gambar</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL  -->
            
            <table class="mt-3 table table-bordered">
                <tr>
                    <th>No Atom</th>
                    <th>Simbol</th>
                    <th>Nama Unsur</th>
                    <th>Massa Atom</th>
                    <th>Elektron</th>
                    <th>Proton</th>
                    <th>Neutron</th>
                    <th>Golongan</th>
                    <th>Keterangan</th>
                    <th>Gambar</th>
                    <th>Opsi</th>
                </tr>
                <?php 
                     $sql = "SELECT u.no_atom, u.simbol, u.nama_unsur, u.massa_atom, u.electron, u.proton, u.neutron, 
                     g.nama_gol,u.keterangan,u.photos
                     from unsur u, golongan g
                     where u.id_gol = g.id_gol";
                     
                     $result = mysqli_query($conn,$sql);
                    while($pst = mysqli_fetch_assoc($result)){
                ?>
                <tr>                 
                    <td><?php echo $pst['no_atom']; ?></td>
                    <td><?php echo $pst['simbol']; ?></td>
                    <td><?php echo $pst['nama_unsur']; ?></td>
                    <td><?php echo $pst['massa_atom']; ?></td>
                    <td><?php echo $pst['electron']; ?></td>
                    <td><?php echo $pst['proton']; ?></td>
                    <td><?php echo $pst['neutron']; ?></td>
                    <td><?php echo $pst['nama_gol']; ?></td>
                    <td><?php echo $pst['keterangan']; ?></td>
                    <td><img src="../images/<?php echo $pst['photos']; ?>" style="width: 100px;" alt="Gambar"></td>
                    <td>
                        <a href="edit-unsur.php?no_atom=<?php echo $pst['no_atom']?>">
                            <button class="btn btn-xs btn-info tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Edit</button>
                        </a>
                        <a href="proses-hapus.php?no_atom=<?php echo $pst['no_atom']?>" title="Hapus Data Ini" onclick="return confirm('Yakin Hapus Data ?')">
                            <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-remove icon-white"></i>Hapus</button>
                        </a> 
                    </td>
                </tr> 
                <?php } ?>
            </table>
        </div>
    </div>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Aplikasi Pembelajaran Kimia ERIzka</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>