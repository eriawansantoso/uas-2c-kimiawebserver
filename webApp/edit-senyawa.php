<?php 
    include("koneksi.php");
    if (!isset($_GET['id_senyawa'])) {
		header('Location:index_senyawa.php');
    }
    $id = $_GET['id_senyawa'];
    $sql = "SELECT * from senyawa where id_senyawa=$id";
	$query = mysqli_query($conn, $sql);
    $unsur = mysqli_fetch_assoc($query);
    if (mysqli_num_rows($query) < 1) {
		die("data tidak ditemukan...");
	}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Unsur</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">CHemistry</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Unsur Kimia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_senyawa.php">Senyawa Kimia<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index_elektrolit.php">Larutan Elektrolit</a>
                    </li>
                </ul>
            </div>
    </nav>

    <div class="container">
        <header>
            <h1><center>Edit Tabel Senyawa<center></h1>
        </header>
        <form action="proses-edit-senyawa.php" method="POST" enctype="multipart/form-data">
            <fieldset>
                            <input type="hidden" class="form-control" id="id_senyawa" name="id_senyawa" value="<?php echo $unsur['id_senyawa'];?>">
                            <div class="form-group">
                                <label for="kation" class="col-form-label">Kation </label>
                                <input type="text" class="form-control" id="kation" name="kation" value="<?php echo $unsur['kation'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="anion" class="col-form-label">Anion </label>
                                <input type="text" class="form-control" id="anion" name="anion" value="<?php echo $unsur['anion'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="rumus_senyawa" class="col-form-label">Rumus Senyawa </label>
                                <input type="text" class="form-control" id="rumus_senyawa" name="rumus_senyawa" value="<?php echo $unsur['rumus_senyawa'];?>" required>
                            </div>
                            <div class="form-group">
                                <label for="nama_senyawa" class="col-form-label">Nama Senyawa </label>
                                <input type="text" class="form-control" id="nama_senyawa" name="nama_senyawa" value="<?php echo $unsur['nama_senyawa'];?>" required>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                            </div>
            </fieldset>
        </form>

    </main>
    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Aplikasi Pembelajaran Kimia ERIzka</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>